import 'package:flutter/material.dart';
import './product_manager.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
        theme: ThemeData(
          brightness: Brightness.light,
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.deepPurple
        ),
        home: Scaffold(
      appBar: AppBar(
        title: Text("Food List"),
      ),
      body: ProductManager("Food Tester"),
    ));
  }
}
